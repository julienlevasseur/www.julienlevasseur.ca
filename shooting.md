---
title: "Shooting"
layout: about
---

![ipsc1](https://julienlevasseur-images.s3.amazonaws.com/20190720_143712.jpg)

![ipsc2](https://julienlevasseur-images.s3.amazonaws.com/20190720_145820.jpg)

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
    google.charts.load("current", {packages:['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
      var data = google.visualization.arrayToDataTable([
        ["Match", "Percentage", { role: "style" }, "Level" ],
//        ["CTO 2018-03-10", 27.63, "silver", "2"],
//        ["CTM 2018-03-31", 40.94, "silver", "2"],
//        ["CTM 2018-05-26", 43.56, "silver", "2"],
//        ["CTJ 2018-11-03", 64.30, "silver", "2"],
//        ["CTM 2018-11-24", 45.59, "silver", "2"],
//        ["CTM 2018-12-16", 43.60, "silver", "2"],
        ["MIC 2019-02-01", 45.61, "lightblue", "3"],
        ["CTM 2019-02-23", 64.84, "silver", "2"],
        ["CTO 2019-03-29", 85.99, "silver", "2"],
        ["CTM 2019-04-27", 53.59, "silver", "2"],
        ["CTO 2019-05-04", 67.66, "silver", "2"],
        ["CTM 2019-05-24", 59.65, "silver", "2"],
        ["CTS 2019-06-22", 100, "silver", "2"],
        ["CTS 2019-07-20", 100, "silver", "2"],
        ["IPSC Canadian Nationals", 52.31, "lightblue", "3"],
        ["CTS 2019-08-17", 86.76, "silver", "2"],
        ["CTM 2019-10-26", 65.26, "silver", "2"],
        ["CTM 2019-11-23", 82.15, "silver", "2"],
        ["CTM 2019-12-21", 61.41, "silver", "2"],
        ["CTJ 2020-01-11", 00.00, "silver", "2"],
        ["CTO 2020-01-18", 87.56, "silver", "2"],
      ]);

      var view = new google.visualization.DataView(data);
      view.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

      var options = {
        title: "Scores in Standard Division",
        width: 800,
        height: 600,
        bar: {groupWidth: "90%"},
        legend: { position: "none" },
      };
      var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
      chart.draw(view, options);
  }
  </script>
<!--<div id="columnchart_values" style="width: 900px; height: 300px;"></div>-->
<div id="columnchart_values"></div>

### Legend

![silver_legend](assets/images/shooting/silver_legend.png) - Level 2 match

![lightblue_legend](assets/images/shooting/lightblue_legend.png) - Level 3 match

**CTM**: Club de Tir de Montréal

**CTO**: Club de Tir de l'Outaouais

**CTS**: Club de Tir de Sandhill

**MIC**: Montréal Indoor Challenge

**[2019 IPSC Canadian Nationals Championship](2019/08/05/2019-IPSC-Canadian-Nationals.html)**

## Videos

[![IPSC Sandhill 2019 07 20 stage1](https://i.ytimg.com/vi/fSUoOJxeYmc/hqdefault.jpg)](https://youtu.be/fSUoOJxeYmc)

[![IPSC Sandhill 2019 07 20 stage2](https://i.ytimg.com/vi/dlnvketQFX0/hqdefault.jpg)](https://youtu.be/dlnvketQFX0)

[![IPSC Sandhill 2019 07 20 stage3](https://i.ytimg.com/vi/ugQHx7O8rXc/hqdefault.jpg)](https://youtu.be/ugQHx7O8rXc)

[![IPSC Sandhill 2019 07 20 stage4](https://i.ytimg.com/vi/NXx-yXtGQkY/hqdefault.jpg)](https://youtu.be/NXx-yXtGQkY)

[![IPSC Sandhill 2019 06 22 stage4](https://i.ytimg.com/vi/5aQyZnhKlBk/hqdefault.jpg)](https://youtu.be/5aQyZnhKlBk)

[![IPSC Sandhill 2019 06 22 stage5](https://i.ytimg.com/vi/6Y96jdZkY30/hqdefault.jpg)](https://youtu.be/6Y96jdZkY30)

![IPSC_intermediate_course](https://julienlevasseur-images.s3.amazonaws.com/IPSC_intermediate_course.jpg)
