job "www" {
  datacenters = ["home"]
  type = "service"

  update {
    stagger      = "30s"
    max_parallel = 1
  }

  group "web" {
    count = 1

    task "docker" {
      driver = "docker"

      config {
        image = "julienlevasseur/www.julienlevasseur.ca:latest"
        network_mode = "host"
      }

      resources {
        cpu    = 100 # MHz
        memory = 384 # MB

        network {
          mbits = 10

          port "http" {
            static = 4000
          }
        }
      }

			service {
			  name = "${NOMAD_JOB_NAME}-internal"
    	  tags = [
					"www",
					"docker",
					"${NOMAD_JOB_NAME}",
					"${NOMAD_ALLOC_NAME}",
					"traefik.enable=true",
          //"traefik.http.routers.www.entrypoints=https",
          //"traefik.http.routers.www.rule=Host(`${NOMAD_JOB_NAME}.julienlevasseur.ca`)",
          "traefik.http.routers.www.service=${NOMAD_JOB_NAME}-internal",
          //"traefik.http.routers.www.tls=true",
				]
    	  port = "http"

			}
		}

//		task "binary" {
//			driver = "raw_exec"
//
//			env {
//				CONSUL_HTTP_TOKEN = "583f8724-5a17-4127-9e53-b1e43571e32d"
//			}
//
//			config {
//    		command = "consul"
//    		args    = ["kv", "put", "traefik/http/services/${NOMAD_JOB_NAME}/loadbalancer/servers/0/url", "home.julienlevasseur.ca:${NOMAD_PORT_http}"]
//  		}
//		}
  }
}
