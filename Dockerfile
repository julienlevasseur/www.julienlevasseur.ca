FROM ruby:2.6.1-alpine

LABEL description="julienlevasseur/www.julienlevasseur.ca"
LABEL maintainer="Julien Levasseur"

RUN mkdir -p /opt/jekyll && \
apk upgrade --update && \
apk add \
gcc \
make \
libatomic \
readline \
readline-dev \
libxml2 \
libxml2-dev \
ncurses-terminfo-base \
ncurses-terminfo \
libxslt \
libxslt-dev \
zlib-dev \
zlib \
ruby \
ruby-dev \
yaml \
yaml-dev \
libffi-dev \
build-base \
ruby-io-console \
ruby-irb \
ruby-dev \
ruby-json \
ruby-rake \
git && \
gem install --no-document \
bigdecimal \
webrick \
redcarpet \
kramdown \
maruku \
rdiscount \
RedCloth liquid \
pygments.rb \
sassc \
safe_yaml \
jekyll-paginate \
jekyll-feed \
jekyll-redirect-from \
json \
rake \
jekyll-seo-tag \
jekyll \
jekyll-sitemap \
i18n \
rouge \
jekyll-feed \
jekyll-sass-converter \
ffi && \
rm -rf /root/src /tmp/* /usr/share/man /var/cache/apk/* && \
apk search --update

COPY ./ /opt/jekyll/

WORKDIR /opt/jekyll
EXPOSE 4000

CMD bundle install && bundle exec jekyll serve
