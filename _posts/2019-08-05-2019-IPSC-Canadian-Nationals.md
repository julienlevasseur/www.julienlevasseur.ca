---
title: "2019 IPSC Canadian Nationals"
layout: "about"
---

## Videos

[![2019 IPSC Canadian Nationals Championship](https://i.ytimg.com/vi/5GmFm_HgGbA/hqdefault.jpg)](https://www.youtube.com/playlist?list=PLnutzSWcwcql1_XBg1SBIVKamNZRp4p4p)

## Match result

![match results](https://julienlevasseur-images.s3.amazonaws.com/2019_IPSC_Canadian_Nationals_Championship_Standard_match.png)

## Stats per stage

![stages results](https://julienlevasseur-images.s3.amazonaws.com/2019_IPSC_Canadian_Nationals_Championship_Standard.png)
