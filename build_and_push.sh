#!/usr/bin/env bash

IMAGE_TAG="$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG"

docker build -t "$IMAGE_TAG" .
echo "$CI_REGISTRY_PASSWORD" | docker login -u "$CI_REGISTRY_USER" --password-stdin  https://index.docker.io/v1/
docker push "$IMAGE_TAG"