#!/usr/bin/env bash

export LC_ALL="C.UTF-8"
export LANG="en_US.UTF-8"
export LANGUAGE="en_US.UTF-8"

sudo apt update
sudo apt install -y \
zlib1g-dev \
make \
gcc \
g++ \
git \
libcurl3 \
ruby2.3 \
ruby2.3-dev

sudo gem install \
bundler:1.15 \
public_suffix \
rake \
ruby_dep:1.3.1 \
html-proofer \
minitest

cd /opt/jekyll || exit

bundle install

export NOKOGIRI_USE_SYSTEM_LIBRARIES="true"
export JEKYLL_ENV="production"

jekyll serve --baseurl "" --detach && htmlproofer ./_site --disable-external --empty-alt-ignore
#jekyll serve --baseurl "" --detach
