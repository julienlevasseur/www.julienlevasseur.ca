---
title: "Rally"
layout: about
---

I started rally racing with the Rallye de la Sarthe in april 2013.
I discovered a wonderful and demanding discipline where I meet wonderful people and made true friends.

Since I started in Road Rallyes, I combined with mixed terrain (with Rallye MTA des Cantons de l'Est) and pure off road rally (Rallye de Parent, organized by the Dakar finisher: Patrick Trahan).

Since the begining of 2019, I also participate in automobile regularity rallyes as copilot.

Interview at the RDS `Histoire de sentiers` show during the `Rallye de Parent`:

[![Histoires de sentier rallye de parent](https://i.ytimg.com/vi/hL-BIl4_VrQ/hqdefault.jpg)](https://www.youtube.com/watch?v=hL-BIl4_VrQ)

## Participations

### 2019

* Rallye-École du Temps des Sucres - Rookie Winner as copilot
 
  ![2019_rallye_ecole_du_temps_des_sucres_results.png](https://julienlevasseur-images.s3.amazonaws.com/2019_rallye_ecole_du_temps_des_sucres_results.png)

* La grosse ride - as copilot

  ![2019_rallye_ecole_du_temps_des_sucres_results.png](https://julienlevasseur-images.s3.amazonaws.com/2019_grosse_ride_results.png)

### 2018

* Rallye de Parent - Finisher (no ranking)

### 2014

* Rallye de la Sarthe
* Rallye des Ch'tis givrés

### 2013

* Rallye des Volcans
* Rallye de la Sarthe
