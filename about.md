---
title: "About"
layout: about
---

Former  SysAdmin, I have developed my development skills over the years by collaborating closely with many dev teams using many languages. Specialist of Cloud  automation, I’m a CI/CD, TDD and Orchestration enthusiast. As I got my skills from others, it’s important to me to share my knowledge and continue to work with great people and face new challenges to continue to grow.

First Hashicorp User Group @Montréal, presenting Terraform: 
![hug_mtl_01_terraform_presentation.jpeg](https://raw.githubusercontent.com/julienlevasseur/jekyll-theme-basically-basic/master/assets/images/about/hug_mtl_01_terraform_presentation.jpeg)

Team work promotion picture at OneSpan:
![esignlive.jpeg](https://raw.githubusercontent.com/julienlevasseur/jekyll-theme-basically-basic/master/assets/images/about/esignlive.jpeg)

Hiring promotion video at Aerial Technologies:

[![Aerial video](https://i.ytimg.com/vi/SRpKDjWyeOg/hqdefault.jpg)](https://www.youtube.com/watch?v=SRpKDjWyeOg)